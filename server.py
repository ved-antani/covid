from collections import namedtuple

from flask import Flask
from flask_caching import Cache
import json
import requests
import folium
from pymongo import MongoClient
from pprint import pprint
from flask_caching import Cache
from folium.features import DivIcon
import numpy as np
from folium.plugins import BeautifyIcon
from folium.plugins import AntPath
import geopandas
from folium.plugins import Search

app = Flask(__name__)
cache = Cache(config={'CACHE_TYPE': 'simple'})
cache.init_app(app)

client = MongoClient('mongodb://localhost:27017/')
db = client.covid
raw_data = db.raw_data
travel_history = db.travel_history
merged_travel = db.merged_travel

@app.route('/')
@cache.cached(timeout=50)
def index():
    print('method call')
    start_coords = (28.7041, 77.1025)
    folium_map = folium.Map(location=start_coords, zoom_start=4)
    #folium.TileLayer('cartodbpositron').add_to(folium_map)
    valid_markers = []
    for m in merged_travel.find():
        for latlon,address in zip(m['travel_markers'],m['travel_address']):
            if latlon != '':
                ll = latlon.split(',')
                ic1 = BeautifyIcon(icon='car', border_color='#b3334f', text_color='#b3334f')
                tip="Patient {} travelled to {} ".format(m['original_patient'],address)
                folium.Marker([float(ll[0]), float(ll[1])],
                              icon=ic1,
                              popup=tip).add_to(folium_map)
                valid_markers.append(tuple([float(ll[0]), float(ll[1])]))

    for r in raw_data.find():
        try:
            ic2 = BeautifyIcon(icon='ambulance', border_color='b3334f', text_color='#b3334f')
            tip = "Patient : {} from {}, {} identified on {} and currently {}".format(r['patientnumber'],r['detectedcity'], r['detectedstate'],r['dateannounced'],r['currentstatus'])
            folium.Marker([float(r['lat']), float(r['lon'])],icon=ic2,popup=tip).add_to(folium_map)
        except ValueError as ve:
            pass

    antpath = AntPath(locations=valid_markers)
    antpath.add_to(folium_map)
    # states = geopandas.read_file(
    #     'india_stages.geojson',
    #     driver='GeoJSON'
    # )
    #
    # stategeo = folium.GeoJson(
    #     states,
    #     name='States',
    #     tooltip=folium.GeoJsonTooltip(
    #         fields=['NAME_1'],
    #         aliases=['State'],
    #         localize=True
    #     )
    # ).add_to(folium_map)
    # statesearch = Search(
    #     layer=stategeo,
    #     geom_type='Polygon',
    #     placeholder='Search for a State',
    #     collapsed=False,
    #     search_label='NAME_1',
    #     weight=3
    # ).add_to(folium_map)

    return folium_map._repr_html_()


if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
