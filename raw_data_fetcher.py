import requests
from pygeocoder import Geocoder
import geocoder
import pandas as pd
import numpy as np
import csv
import os
from pprint import pprint
from pymongo import MongoClient
import time

def import_raw_data():
    client = MongoClient('mongodb://localhost:27017/')
    db = client.covid
    raw_data = db.raw_data
    #delete all raws
    raw_data.delete_many({})

    #get raw data
    url = "https://api.covid19india.org/raw_data.json"
    r=requests.get(url)
    full_data=r.json()
    for g in full_data['raw_data']:
        try:
            insert_result = raw_data.insert_one(g)
            pprint(insert_result)

        except Exception as e:
            print (e)
    pprint(raw_data.count({}))

def import_travel_history():
    client = MongoClient('mongodb://localhost:27017/')
    db = client.covid
    travel_history = db.travel_history
    #delete all raws
    travel_history.delete_many({})

    #get raw data
    url = "https://api.covid19india.org/travel_history.json"
    r=requests.get(url)
    full_data=r.json()
    for g in full_data['travel_history']:
        try:
            insert_result = travel_history.insert_one(g)
            pprint(insert_result)

        except Exception as e:
            print (e)
    pprint(travel_history.count({}))

def trim_all_the_columns(df):
    trim_strings = lambda x: x.strip() if type(x) is str else x
    return df.applymap(trim_strings)

def get_raw_data_to_csv():
    url = "https://api.covid19india.org/raw_data.json"
    r = requests.get(url)
    full_data = r.json()
    f=csv.writer(open("raw_data.csv",'w'))
    #write header
    f.writerow(["agebracket","backupnotes","contractedfromwhichpatientsuspected","currentstatus","dateannounced","detectedcity","detecteddistrict","detectedstate","estimatedonsetdate","gender","nationality","notes","patientnumber","source1","source2","source3","statepatientnumber","statuschangedate"])

    for x in full_data['raw_data']:
        f.writerow([x["agebracket"],x["backupnotes"],x["contractedfromwhichpatientsuspected"],x["currentstatus"],x["dateannounced"],x["detectedcity"],x["detecteddistrict"],x["detectedstate"],x["estimatedonsetdate"],x["gender"],x["nationality"],x["notes"],x["patientnumber"],x["source1"],x["source2"],x["source3"],x["statepatientnumber"],x["statuschangedate"]])

    df = pd.read_csv('raw_data.csv')
    df.columns = df.columns.str.strip()
    return trim_all_the_columns(df)

def get_travel_history_to_csv():
    url = "https://api.covid19india.org/travel_history.json"
    r = requests.get(url)
    full_data = r.json()
    f=csv.writer(open("travel_history.csv",'w'))
    #write header
    f.writerow(["address","datasource","entryid","latlong","modeoftravel","pid","placename","timefrom","timeto","type"])
    for x in full_data['travel_history']:
        f.writerow([x["address"],x["datasource"],x["entryid"],x["latlong"],x["modeoftravel"],x["pid"],x["placename"],x["timefrom"],x["timeto"],x["type"]])

    df = pd.read_csv('travel_history.csv')
    #print(df)
    df.columns = df.columns.str.strip()
    return trim_all_the_columns(df)

def merge_travel_details():
    if os.path.exists('merge.csv'):
        print ("file exists")
        return pd.read_csv('merge.csv')
    else:
        print("file doesnt exists")
        raw_df = get_raw_data_to_csv()
        travel_df = get_travel_history_to_csv()

        for i,r in raw_df.iterrows():
            pid = r['patientnumber']
            query_string= 'pid=="P{}"'.format(str(pid))
            result_travel = travel_df.query(query_string)
            markers=[]
            if not result_travel.empty:
                markers.append(result_travel['latlong'].values)

            # #Add my own marker
            # result_address = geocoder.osm(str(r['detectedcity']) + ' ' + str(r['detecteddistrict']) + ' ' + str(r['detectedstate']) + ' India')
            #
            # if result_address:
            #     print(str(pid) + ' --- ' + str(result_address.osm['x']) + ',' + str(result_address.osm['y']) + ' ~~ ' + str(r['detectedcity']) + ' ' + str(r['detecteddistrict']) + ' ' + str(r['detectedstate']) + ' India')
            #     markers.append(['{},{}'.format(result_address.osm['x'],result_address.osm['y'])])

            raw_df.loc[raw_df.eval('patientnumber=="{}"'.format(str(pid))), 'markers'] = pd.Series([markers] * len(raw_df))

        raw_df.to_csv('merge.csv')
        return raw_df


def merge_travel_history():
    client = MongoClient('mongodb://localhost:27017/')
    db = client.covid
    raw_data = db.raw_data
    travel_history = db.travel_history
    patient_record = []

    for r in raw_data.find():
        markers = []
        travel_address = []
        zero_marker = []

        pid = r['patientnumber']
        for t in travel_history.find({"pid":"P"+str(pid)}):
            pprint(str(pid) +'---' + str(travel_history.count_documents({"pid":"P"+str(pid)})))
            markers.append(t['latlong'])
            travel_address.append(t['address'])

        # try:
        #     result_address = geocoder.osm(str(r['detectedcity']) + ' ' + str(r['detecteddistrict']) + ' ' + str(r['detectedstate']) + ' India')
        #     zero_marker.append(str(result_address.osm['x']) + ',' + str(result_address.osm['x']))
        #     print(str(pid) + ' --- ' )
        # except TypeError as te:
        #     zero_marker.append('')

        patient_record.append({'gender': r['gender'],
                               'original_patient': r['patientnumber'],
                               'current_status:': r['currentstatus'],
                               'detected_at': r['detectedcity'] + ',' + r['detectedstate'],
                               # 'zero_marker':zero_marker,
                               'travel_markers': markers,
                               'travel_address': travel_address
                               })
    merged_travel = db.merged_travel
    #first remove all records
    merged_travel.delete_many({})
    insert_result = merged_travel.insert_many(patient_record)
    #pprint(insert_result.inserted_ids)
    return patient_record

def geo_locate_raw_data():
    #export GOOGLE_API_KEY = AIzaSyCqA6yYICIupN1Vcwqy5qyLV1 - svTz1DBo
    client = MongoClient('mongodb://localhost:27017/')
    db = client.covid
    raw_data = db.raw_data
    travel_history = db.travel_history
    patient_record = []

    for r in raw_data.find():
        time.sleep(2)
        try:
            print(r['patientnumber'])
            #result_address = geocoder.osm(str(r['detectedcity']) + ' ' + str(r['detecteddistrict']) + ' ' + str(r['detectedstate']) + ' India')
            result_address = geocoder.google(str(r['detectedcity']) + str(r['detectedstate']) +  ' India')
            raw_data.update_one({
                '_id': r['_id']
            }, {
                '$set': {
                    'lat': result_address.latlng[0],
                    'lon': result_address.latlng[1]
                }
            }, upsert=False)
        except TypeError as te:
            print(r['patientnumber'])
            print(te)
            raw_data.update_one({
                '_id': r['_id']
            }, {
                '$set': {
                    'lat': '',
                    'lon': ''
                }
            }, upsert=False)

def clean_create_data():
    import_raw_data()
    import_travel_history()
    merge_travel_history()
    geo_locate_raw_data()




if __name__=="__main__":
    clean_create_data()